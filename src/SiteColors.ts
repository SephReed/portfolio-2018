const Colors = {
	linkColor: "#36807a",
	// timelineStroke: "#2F5451",
	timelineStroke: "#36807a",
	timelineStrokeWidth: 1,
	timelineFill: "rgba(100, 100, 100, 0.3)",
	timelineTitleBg: "#831F34",
	contactTitleBg: "#36807a",

}

// const Colors = {
// 	linkColor: "#00FFC0",
// 	timelineStroke: "#00FFC0",
// 	timelineStrokeWidth: 1,
// 	timelineTitleBg: "#ff0048",
// 	contactTitleBg: "#d4ff61",
// }

export default Colors;
