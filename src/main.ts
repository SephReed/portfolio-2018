import { create, br, div, span } from "el-tool";
import hideOutlines from "./Util/hideOutlines";
import * as Sections from "./Sections";
import {addFirstLoadThrashingClass} from "./Util/onThrashingEnd";
import addBrowserClassToBody from "./Util/addBrowserClassToBody"

import SiteColors from "./SiteColors";

import "./main.scss";
import "./mediaQueries.scss";
import "./ClipPaths.scss";
// import "./Colors.scss";

const colorsStyle = document.createElement("style");
colorsStyle.innerHTML = [
	":root {",
	...Array.from(Object.keys(SiteColors)).map((key) => `--${key}: ${SiteColors[key]};`),
	"}",
].join("\n");
document.head.appendChild(colorsStyle);


const dividers: HTMLElement[] = [];

hideOutlines();
addBrowserClassToBody();
const app = div("App", [
	() => { dividers.push(div("Divider reverse")); return dividers[dividers.length-1];},
	Sections.genHeader(),
	() => { dividers.push(div("Divider")); return dividers[dividers.length-1];},
	Sections.genWelcome(),
	(new Sections.Timeline()).domNode,
	Sections.genGoToContact(),
	Sections.genContact(),
]);


export default function () {
	
	document.addEventListener("DOMContentLoaded", () => {
		document.getElementById("AppWrapper").appendChild(app);

		addFirstLoadThrashingClass();
		setTimeout(
			() => document.body.classList.remove("--skip_animations"),
			1500,
		);

		const intervalMs = 150;
		const loopLength = 30* 1000;
		let time = 0;
		setInterval(() => {
			time += intervalMs;
			time %= loopLength;
			const dividerBgOffset = time/loopLength * 100;
			dividers.forEach((divider) => divider.style.backgroundPosition = `${dividerBgOffset}% 0%`);
		}, intervalMs)
	})
}
