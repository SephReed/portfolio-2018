import { a, create, br, div, span, Innards } from "el-tool";
import SiteColors from "../SiteColors";
import ScalableCurfAug from "../Util/ScalableCurfAug";

import "./resume.scss";

const colorsStyle = document.createElement("style");
colorsStyle.innerHTML = [
	":root {",
	...Array.from(Object.keys(SiteColors)).map((key) => `--${key}: ${SiteColors[key]};`),
	"}",
].join("\n");
document.head.appendChild(colorsStyle);

const createLink = (href) => a({href: `http://${href}`}, href);

const employmentHistory: Array<{positions: Innards, company: Innards, time: Innards, info: Innards}> = [
{
	positions: ["Contract CTO,", br(), "Full Stack"],
	company: ["Choose Health", br(), createLink("sephreed.me#ChooseHealthDev")],
	time: "Mar 2019 - Present",
	info: [{hackableHTML: `John A. Entwistle - ceo<small>jae@coder.com</small><br>
		Joanne Kimmilnes - designer<small>info@joannekimmilnes.com</small>`}],
},{
	positions: ["Frontend Dev,", br(), "UX Team"],
	company: ["Coder Technologies", br(), createLink("sephreed.me#CoderComDev")],
	time: "Jan-Oct 2018",
	info: [{hackableHTML: `John A. Entwistle - ceo<small>jae@coder.com</small><br>
		Joanne Kimmilnes - designer<small>info@joannekimmilnes.com</small>`}],
},{
	positions: ["Co-designer,", br(), "Fabrication Lead"],
	company: ["Austin Artistic Reconstruction LLC", br(), createLink("sephreed.me#Cononagon")],
	time: "Jan-Dec 2018",
	info: [{hackableHTML: `Adam Rice - board member<small>adamrice@8stars.org</small>`}],
},
// {
// 	positions: ["Full Stack Dev,", br(), "Fabricator"],
// 	company: ["Contract Work", br(), "(Custom interactive light)", br(), createLink("sephreed.me#DadLight")],
// 	time: "Jan-Aug 2017",
// 	info: [{hackableHTML: `Sally Hall - client<small>sally.hall@austincenterfordesign.com</small>`}],
// },
{
	positions: ["Co-designer,", br(), "Fabrication Lead"],
	company: ["Austin Artistic Reconstruction LLC", br(), createLink("sephreed.me#PlaywoodPalace")],
	time: "Jan-Dec 2017",
	info: [{hackableHTML: `Stephanie Vyborny - artist facilitator<small>the.lost.muse@gmail.com</small>`}],
},{
	positions: ["Web Dev,", br(), "3D Modeling"],
	company: ["Stash Crypto", br(), createLink("sephreed.me#Stash")],
	time: "2016",
	info: [{hackableHTML: `Anonymity desired`}],
},{
	positions: ["Full Stack Dev"],
	company: ["Realme App", br(), "(Design School Final Project)"],
	time: "Spring 2016",
	info: [{hackableHTML: `Misty Nickle - client<small>mistynickle@gmail.com</small>`}],
},{
	positions: ["Instructor,", br(), "Fabricator"],
	company: ["Austin Tinkering School"],
	time: "Summer 2016",
	info: [{hackableHTML: `Kami Wilt - owner<small>kami@austin.tinkeringschool.com</small>`}],
},{
	positions: ["Forest Service,", br(), "Team Leading"],
	company: ["	Northwest Youth Corps.", br(), "Leadership Development"],
	time: "Fall 2014",
	info: [{hackableHTML: `Jon Zintell - leader<small>jon@nwyouthcorps.org<small>`}],
},
]


const skills: Array<[number, string]> = [
	[1, "2020 - Rust"],
	[2, "2019 - AWS / EC2"],
	[2, "2019 - Mobile App / Nativescript"],
	[2, "2019 - VST / VST2 / JUCE"],
	[2, "2019 - Sketch"],
	[3, "2018 - Typescript"],
	[2, "2018 - Sass"],
	[3, "2018 - React / Helium / State SPAs"],
	[3, "2018 - Webpack / Fuse Box"],
	[2, "2018 - Arduino / FastLED / C"],
	[1, "2017 - Circuitry"],
	[2, "2017 - WebWorkers"],
	[2, "2017 - Audio DSP"],
	[3, "2017 - Volunteer Leadership"],
	[3, "2017 - Project Management"],
	[3, "2017 - Web Audio API"],
	[2, "2016 - Basic Cryptology (AES-EBC)"],
	[2, "2016 - Blender 3d"],
	[3, "2016 - Teaching"],
	[2, "2016 - Three.js / WebGl"],
	[2, "2016 - Mapbox / Geolocation API"],
	[2, "2016 - Home Renovation"],
	[2, "2014 - Carpentry"],
	[3, "2014 - Team Leadership"],
	[3, "2014 - Bitwig (DAW)"],
	[3, "2013 - 🎓BS in Computer Science from Portland State University"],
	[2, "2012 - Cooking"],
	[1, "2012 - Python"],
	[3, "2012 - Svg / Vector Art"],
	[2, "2012 - SQL"],
	[3, "2011 - CSS"],
	[2, "2010 - Jazz Theory"],
	[2, "2010 - PHP"],
	[3, "2010 - Javascript"],
	[2, "2009 - Fire Performance"],
	[2, "2009 - C++"],
	[2, "2009 - Bash"],
	[2, "2008 - Java"],
	[3, "2007 - Photoshop / Gimp"],
	[3, "2007 - SketchUp (CAD)"],
	// [1, "2007 - Auto-Cad"],
	[3, "2007 - Digital Audio Workspaces"],
	[2, "2004 - 3d Modeling"],
	[2, "2004 - Guitar"],
	[3, "2002 - HTML"],
	[3, "2000 - Piano"],
	[1, "2000 - WYSIWYG Java"],
]


const forPrint = window.location.search.indexOf("PrintVersion") !== -1;

let table: HTMLElement;
let portfolio: HTMLElement;
const app = div(`Resume ${forPrint ? "--variant_print_version" : ""}`, [
	div("LeftCol", [
		div("Name", [
			div("MyName", "Seph Reed"),
			"legal name: Scott Jaromin",
		]),
		div("Contact", [
		`ShadyWillowCreek@gmail`,br(),
		`(737)-529-5031`,
		]),
		div("Positions", [
			{hackableHTML:
				`Full Stack Developer
				Project Lead
				UI/UX Designer
				2D/3D Digital Artist
				Fabricator
				Producer/Jazz Pianist`.split("\n").join("<br>")
			}
		]),
		div("SelfIntro", 
			`Hey there 👋.  I'm Seph: a slightly peculiar, widely curious, intrigue driven dude.  If you want to know more, 
			"INTJ" is my Myers-Briggs personality type; it can give you better insight than this bio will.\n

			My current motivation is to work in tech, preferably on projects adjacent to art, futurism, or social good; eventually I plan to run a tech company of my own.

			My expectations from an employer revolve around professionalism, open-mindedness, the potential for growth, and respect. 
			As long as these criteria are met, I'll find comfort in my work.

			In my free-time, I like to work on interactive art projects, make music, play with fire, and code.

			As a final note: these lists of skills and jobs display I'm able to follow instructions, but my truest skill is
			how I think: creative, logical, outside the box.  Hire me for that.
			
			`.split("\n\n")
			.map((blurb) => div("Blurb", blurb)),
		)
		// div("SelfIntro", 
		// 	`Hello.  My name is Seph and I'm trying to understand the system as a whole. My interests and skills are all over the place, I rarely consider odd jobs a waste of time, and I have a habit of being able to fill suprising roles.  In all fields, I'm a relentless learner.

		// 	My core values are self-respect, humor, integrity, self-awareness, and competency. My personality type is somewhere between INTJ and ENTJ.

		// 	Ideally, I would like an employer that is willing to push boundaries and strives to create a symbiotic relationship between itself and its customers.

		// 	Idealistically, I would like to work with art, music, and those kinds of things which impact people in deep, long lasting ways.

		// 	Overall, I'd just like to solve some problems and have a good time while I'm at it.`.split("\n\n")
		// 	.map((blurb) => div("Blurb", blurb)),
		// )
	]),
	div("RightCol", [
		div("PrintLink", [
			"Printable version: ",
			a({href: "http://sephreed.me/SephReed-Resume(printer).pdf"}, "sephreed.me/SephReed-Resume(printer).pdf"),
		]),
		div("Title", "Employment History".toUpperCase()),
		table = div("EmploymentTable", [
			// ...["Positions", "Company / Info Link", "Time", "Contact / References"].map((title) => div("TableHeader", title)),
			...["Positions", "Company / Info Link", "Time"].map((title) => div("TableHeader", title)),
			div("Spacer header"),
			...(() => {
				const rows = [];
				employmentHistory.forEach((employment, index) => {
					if (index > 0) { rows.push(div("Spacer")); }
					const datumList = Object.keys(employment).map((prop) => div("Datum", employment[prop]));
					datumList.pop();
					rows.push(div("Employment", datumList));
				});
				return rows;
			})(),
		]),
		portfolio = div("Portfolio", [
			"My portfolio showcases everything above and below",
			a({href: "http://SephReed.me"}, "http://SephReed.me"),
		]),
		div("Skills", [
			div("Title", "Skills Timeline".toUpperCase()),
			div("Desc", "The following is a timeline of various skills I've acquired. The date denotes the time of my first project involving the skill. The stars represent beginner, intermediate, and expert levels."),
			div("SkillList", skills.map((skill) => {
				let stars: string;
				switch(skill[0]) {
					case 1: stars = "<span>&#9733;</span>&#9734;&#9734"; break;
					case 2: stars = "<span>&#9733;&#9733;</span>&#9734"; break;
					case 3: stars = "<span>&#9733;&#9733;&#9733</span>"; break;
				}
				return div("Skill", [
					span("Stars", [{hackableHTML: stars}]),
					skill[1],
				]);
			}))
		])
	]),
]);

new ScalableCurfAug(table, {
	curfNe: {width: 25, height: 25},
	curfSw: {width: 12.5, height: 12.5},
	strokeStyle:  forPrint ? "black" : "#0db08d" || SiteColors.timelineStroke,
	strokeWidth: 2,
	fillStyle: forPrint ? null : "#272b2b" || SiteColors.timelineFill,
});

new ScalableCurfAug(portfolio, {
	curfNw: {width: 12.5, height: 12.5},
	curfNe: {width: 12.5, height: 12.5},
	curfSw: {width: 12.5, height: 12.5},
	curfSe: {width: 12.5, height: 12.5},
	fillStyle: forPrint ? null : "#0db08d" || SiteColors.timelineStroke,
	strokeStyle: forPrint ? SiteColors.timelineStroke : null,
	strokeWidth: forPrint ? 3 : null,
});


export default function () {
	document.addEventListener("DOMContentLoaded", () => {
		document.getElementById("AppWrapper").appendChild(app);
	})
}
