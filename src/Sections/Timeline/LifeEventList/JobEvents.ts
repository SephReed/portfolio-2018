import {div} from "el-tool";
import {ILifeEvent, twoColLayout, extLink, fromMD} from "./LifeEvent"


const JobEvents: ILifeEvent[] = [];


/*****************
*.  CODER
******************/
JobEvents.push({
	id: "ChooseHealthDev",
	startDate: new Date("Mar 10 2019"),
	// endDate: new Date("Sep 25 2018"),
	tags: ["featured", "work", "development", "design"],
	title: "Contract CTO at Choose Health",
	desc: () => twoColLayout(
		[
			// { hackableHTML: extLink(
			// 	`<img style="width: 300px;" class="" src="${require('../../../../assets/ChooseHealth-Kit.png')}">`,
				
			// )},
			{ hackableHTML: extLink(
				`<img style="border-radius: 10%" src="${require('../../../../assets/ChooseHealth-App-Screencap.gif')}">`,
				"https://choosehealth.io/survey/intro", 
			)},
		], 
		fromMD(`
**Skills:** Full-Stack, Single Page App, Native Mobile App, Rest Server, Service Integrations, Typescript, AWS EC2, Nativescript, Stripe

----------
**Info:** Choose Health is an at home blood testing service which allows users to track their their health and get insights
into potential improvements or current risks.  The service includes a subscription payment model that communicates with
a blood testing lab to send finger-prick blood tests.  It also includes a mobile app which lets users know when their kit or test results are
ready, and help them explore said results.
<br><br>
As the sole developer, my responsibilities included:
<br><br>
- Inheriting and maintaining a web stack built from classic HTML/JS on frontend, and Loopback with Typescript on backend.
- Building from scratch a second stack which uses Typescript with Helium for frontend, and a custom extension of Feathers js/GraphQL for backend.
- Integrating with services such as Stripe, Autopilot, Drip, Firebase, Postmark, Shippo, and more.
- Building a mobile app using NativeScript of the Typescript flavor.
- Managing AWS EC2 servers hosting our sites, rest apis, and https credentials.


----------
<div class="Links">
	${extLink(
		"ChooseHealth.io", 
		"https://choosehealth.io/survey/intro",
	)}
</div>
		`),
	),
});




/*****************
*.  CODER
******************/
JobEvents.push({
	id: "CoderComDev",
	startDate: new Date("Jan 15 2018"),
	endDate: new Date("Sep 25 2018"),
	tags: ["featured", "work", "development", "design", "audio"],
	title: "Front-End Developer at Coder Technologies",
	desc: () => twoColLayout(
		[
			{ hackableHTML: extLink(
				`<img style="width: 300px;" class="" src="${require('../../../../assets/CoderLogo(square).png')}">`,
				"http://coder.com", 
			)},
			{ hackableHTML: extLink(
				`<img class="--clip_curf_all_med" src="${require('../../../../assets/CoderInterior.jpg')}">`,
				"https://twitter.com/CoderHQ/status/1027664822891163653", 
			)},
		], 
		fromMD(`
**Skills:** Typescript, Sass, React, Node.js, Npm, Git, Vanilla Js, Audio Editing, Documentation, Storyboarding

----------
**Info:** When I started at Coder there were less than a dozen employees, so I got to fill many roles besides front-end developer.  
Our mission was to create a collaborative cloud based software development suite, including in browser 
IDE and remote compilation/storage/hosting. Some of the projects I did include:
<br><br>
- Site wide theming, procedurally generated from Textmate/Bash themes (see Lumas)
- Many internal pages of outstanding quality (as said by the designers and QA)
- Major site components such as Button, Link, EditableText, ValidatorInput, PageTween, MinimizableDiv, and Modal
- A keyboard navigation system for the IDE
- Background/intro/outro music for company promo/tutorial vids
- Original concepting for landing page
- Some major influences in our workflow and timeline
- Head of Easter Eggs
- A few very nice blog posts
----------
<div class="Links">
	${extLink(
		"Coder.com", 
		"http://coder.com",
	)}
	<br>
	${extLink(
		"Watch product video I made the music for", 
		"https://www.youtube.com/watch?v=CMKRbc8DpVs",
	)}
	<br>
	${extLink(
		"Link to song from video above", 
		"https://soundcloud.com/thumbz/forestbackground",
	)}
</div>
		`),
	),
});

/*********************
*   DAD LIGHT
**********************/
JobEvents.push({
	id: "DadLight",
	startDate: new Date("Jan 21 2017"),
	endDate: new Date("Aug 11 2017"),
	tags: ["featured", "work", "development", "fabrication", "audio"],
	title: `Independent Contractor for Project "Dad Light"`,
	desc: () => twoColLayout(
		[
			{ hackableHTML: extLink(
				`<img class="--bordered" src="${require('../../../../assets/DadLight-Glamor(loop).gif')}">`,
				"/assets/DadLight-Glamor(loop).gif", 
			)},
			{ hackableHTML: extLink(
				`<img class="--clip_curf_nw_se --shorten" src="${require('../../../../assets/DadLight-Internal.jpg')}">`,
				"/assets/DadLight-Internal.jpg", 
			)},
			// { hackableHTML: extLink(
			// 	`<img class="--shorten" src="${require('../../../../assets/DadLight-App.png')}">`,
			// 	"/assets/DadLight-App.png", 
			// )},
		],
		// 
		fromMD(`
**Skills:** Web Workers, Linux (Raspian), Chip Design, Node Js, FFT (Fast Fourier Transform), Carpentry, App Design

----

**Info:** Sally Hall was just finishing up her classes at Austin Center for Design and had a wonderful idea of a gift to make her father.  The basic premise was this:  Her father was an audiologist, so she wanted to make him a ceiling light which responded to sound similar to the way the hairs in our ears do while playing music.  She spread this idea broadly, looking for someone who could make it happen (on a college student's budget), and found me.  We met over coffee and discussed its feasibility, then I used sketchup to design the project alongside her.  What we came up with was a seamless box containing a raspberry pi, break-out board, and power supply that would hang from the ceiling.  Extending out of this box was twelve hanging jar lamps with fairy lights inside, each lamp representing a tone from the western 12 tone scale.  In order to make the lights play music, I developed a webapp which was served from the pi, in which Sally could choose songs, run patterns, turn on mic input, and control things like brightness, on/off, and fade time.  The hardest parts of this project were getting the FFT to be performant, and designing the 12 female-USBs breakout board which had to use MOSFETs to step up the pi's 3v3 GPIOs to the 5v power supply for the lights.
		`),
	),
});



/*********************
*   STASH CRYPTO
**********************/
JobEvents.push({
	id: "Stash",
	startDate: new Date("Oct 8 2016"),
	tags: ["featured", "work", "development", "design"],
	title: "Web Developer at Stash Crypto Inc",
	desc: () => twoColLayout(
		[
			{ hackableHTML: extLink(
				`<img class="" src="${require('../../../../assets/StashLogo.png')}">`,
				"https://stashcrypto.com", 
			)},
			{ hackableHTML: extLink(
				`<img class="--bordered --add_bg_light" src="http://sephreed.github.io/portfolio/public/images/stash_node_site.gif">`,
				"http://sephreed.github.io/stash_node/index.html", 
			)},
			{ hackableHTML: extLink(
				`<img class="--bordered --add_bg_light" src="http://sephreed.github.io/portfolio/public/images/digiden_loop.gif">`,
				"http://sephreed.github.io/Digiden/index.html", 
			)},
		],
		// 
		fromMD(`
**Skills:** Vanilla Js, 3d Modeling, Local Storage, Security, Cryptology

----

**Info:** I met the CEO of Stash Crypto at a crypto-currency meetup held at the Factom offices, and we hit it off pretty well.  He happened to be in need of a web developer at the time.  The company direction was often tumultuous, leading ultimately to the disposal of the company identity in an attempt to mimic Apple (a common story in start-ups).  I made a 3d model of our original logo and our flagship product, as well as a website for an internet alliance they were attempting to start, prototypes for the digital identity search engine (with AES-EBC encrypted local storage of searches) and the product landing page (which includes the product 3d renders).  While working there, I started to really study security and cryptology, specifically through the cryptopals challenges.

---

<div class="Links">
	${extLink(
		"StashCrypto.com", 
		"https://stashcrypto.com",
	)}
	<br>
	${extLink(
		"View product page prototype", 
		"http://sephreed.github.io/stash_node/index.html",
	)}
	<br>
	${extLink(
		"View digital identity search engine prototype", 
		"http://sephreed.github.io/Digiden/index.html",
	)}
	<br>
	${extLink(
		"Cryptopals crypto challenges", 
		"https://cryptopals.com/",
	)}
</div>
		`),
	),
});

// /*********************
// *   TEMPLATE
// **********************/
// JobEvents.push({
// 	id: "Stash",
// 	startDate: new Date("Oct 8 2016"),
// 	tags: ["featured", "work", "development", "design"],
// 	title: "",
// 	desc: twoColLayout(
// 		[
// 			{ hackableHTML: extLink(
// 				`<img class="--clip_curf_all_sm --add_bg_light" src="${require('../../../../assets/StashLogo.png')}">`,
// 				"https://stashcrypto.com", 
// 			)},
// 		],
// 		// 
// 		fromMD(`
// **Skills:** 

// ----

// **Info:** 

// ---

// <div class="Links">
// 	${extLink(
// 		"StashCrypto.com", 
// 		"https://stashcrypto.com",
// 	)}
// </div>
// 		`),
// 	),
// });

export default JobEvents;