import {div} from "el-tool";
import {ILifeEvent, twoColLayout, extLink, fromMD} from "./LifeEvent"


const CodeEventList: ILifeEvent[] = [];



/*********************
*   THIS PORTFOLIO
**********************/
CodeEventList.push({
	id: "ElPortfolio",
	startDate: new Date("Oct 3 2018"),
	tags: ["featured", "development", "design", "imagery", "writing", "interactive"],
	title: "This Portfolio",
	desc: () => [
		fromMD(`
The web-app you're currently looking at is meant to exemplify my take on modern visual and systems design, as well as an excuse to do some fun stuff I wouldn't normally do at work.  Everything on this page was created by me from scratch, from the interactive art board at the top of the page, to the grid tools which keep all items on the timeline flush with the grid, to the mono-line icons at the bottom of the page in the contact area.  The site was coded in TypeScript and Sass, rendered with "el-tool", and bundled with webpack.  It is also fully responsive, and has been tested on Firefox, Chrome, and Safari.

-----

<div class="Links">
	${extLink(
		"PDF version of this portfolio", 
		"http://sephreed.me/SephReed-Portfolio.pdf",
	)}
	<br>
	${extLink(
		"Link to Resume", 
		"http://sephreed.me/SephReed-Resume.pdf",
	)}
	<br>
	${extLink(
		"Link to Resume (printer friendly version)", 
		"http://sephreed.me/SephReed-Resume(printer).pdf",
	)}
</div>
`
		),
	],
});



// /*********************
// *   NPM Tools
// **********************/
// CodeEventList.push({
// 	id: "NobleJs",
// 	startDate: new Date("Jul 1 2018"),
// 	tags: ["featured", "development", "writing"],
// 	title: "Noble.js",
// 	desc: fromMD(
// `Noble.js is a modular web toolkit which has already shown some very promising results.  The current modules include:

// - el-tool: a web rendering tersifier which can render interactive sites faster than any other client side renderer (including react, vue, and angular).  It has less than 250 lines of unminified/commented code.
// - vanilla-observables: similar to mobx or redux, this tool adds the ability to observe properties on a class, or just create an observable store.  It has less than 100 lines of unminified/commented code which could have been run in 2008.
// - observable-location(unpublished): window.location, but with observable properties.  Includes a helper function for generating non-redirecting ADA compliant links. It has less than 100 lines of unminified/commented code.
// - omject: an abstract base class meant for object model systems.  Replaces globals and pass-downs by giving classes knowledge of their parent class.  Allows for bubbling events upwards, or use of "getAncestor(ClassType)" to access larger scopes directly.  It has less than 125 lines of unminified/commented code.
// - disinteractive-observer(unpublished): allows for the ability to observe whether or not an element is disinteractiveed on page.
// - vanilla-component(unpublished): combines all of the above to make an HTMLElement manager that atatches/detaches observers on show/hide, can have observable properties, and renders incredibly fast.

// The objective of this toolkit is to illustrate just how bloated and overly complicated most of popular tools are these days.  It is named after noble gasses beacuse they are non-reactive.
// `),
// });


/*********************
*   PINE PORTFOLIO
**********************/
CodeEventList.push({
	id: "PINEPortfolio",
	startDate: new Date("Jan 15 2018"),
	tags: ["featured", "development", "design", "imagery", "writing", "3d", "interactive"],
	title: "Previous Portfolio",
	desc: () => twoColLayout(
		{ hackableHTML: extLink(
			`<img class="--clip_curf_ne_sw" src="http://sephreed.github.io/public/images/PortfolioScreen.png">`,
			"http://sephreed.github.io/portfolio", 
		)}, 
		fromMD(`
**Skills:** Vanilla JavaScript (no outside frameworks), Three.js, Canvas

-----

**Info:** While there's no doubt that this new portfolio is much more professional, I still can't decide whether I personally like it more than my old one.  The old one was built off my first web framework (called PINE), and has a ton of really interactive qualities to it that just make me smile.  There's a manipulable 3d model at the top, some classic bumpy gradients, a 3d star field, and some high-res background nonsense.  Worth checking out IMO.

-----

<div class="Links">
	${extLink(
		"Check out my last Portfolio", 
		"http://sephreed.github.io/portfolio/index.html",
	)}
</div>
		`)
	),
});


/*********************
*   DOCURIZER
**********************/
CodeEventList.push({
	id: "Docurizer",
	startDate: new Date("Oct 20 2017"),
	tags: ["featured", "development", "design"],
	title: "Docurizer and Scopifier",
	desc: () => twoColLayout(
		{ hackableHTML: extLink(
			`<img class="--bordered" src="http://sephreed.github.io/portfolio/public/images/Docurizer_overview.gif">`,
			"http://sephreed.github.io/portfolio/Docurizer_Render/index.html?file=main.cpp.html&ov=inc", 
		)}, 
		fromMD(`
**Skills:** Node.js, Code Parsing, Grammars, C++, RegExp, Documentation

-----

**Info:** In order to make sense out of an undocumented open source project, I built these two tools to parse the project and automatically generate docs based off any comments it could find laying around.  A year later, I discovered that when I created Scopifier I'd inadvertantly reinvented grammars.  As for Docurizer, it takes a symbolic tree generated by applying a grammar, reorganizes the information, then renders it as html.  

-----

<div class="Links">
	${extLink(
		"View Docurizers rendered output (try inspecting elements)", 
		"http://sephreed.github.io/portfolio/Docurizer_Render/index.html?file=main.cpp.html&ov=inc",
	)}
	<br>
	${extLink(
		"View C++ grammar for Scopifier", 
		"https://github.com/SephReed/Servers/blob/5e7984405e47587f234679b8bc0e114824e7da13/Docurizer/libs/Scoperizer/scope_defs/cpp/scopes.js",
	)}
	<br>
	${extLink(
		"View project source code", 
		"https://github.com/SephReed/Servers/tree/master/Docurizer",
	)}
</div>
		`),
	),
});



/*********************
*   FLAT WORLD 
**********************/
CodeEventList.push({
	id: "FlatWorld",
	startDate: new Date("Jul 3, 2017"),
	endDate: new Date("Sep 14, 2017"),
	tags: ["featured", "development", "design", "imagery", "interactive"],
	title: "FLAT.World",
	desc: () => twoColLayout(
		{ hackableHTML: extLink(
			`<img class="--bordered" src="http://sephreed.github.io/portfolio/public/images/flatWorld_loop.gif">`,
			"http://sephreed.github.io/FlatStory/index.html", 
		)}, 
		fromMD(`
**Skills:** Single Page Web App, Node.js Servers, HTML5 Canvas

----

**Info:** FLAT.World is an open source in browser 2d game maker. It's very similar to RPG Maker 2000 which was the application I first learned to program in, and I'm hoping to mimic its style that makes coding feel more accessible to new comers. I'm also hoping to use it to make something like a game.

---

<div class="Links">
	${extLink(
		"Play with live demo (no server access)", 
		"http://sephreed.github.io/FlatStory/index.html",
	)}
	<br>
	${extLink(
		"View source code", 
		"https://github.com/SephReed/SephReed.github.io/tree/master/FlatStory",
	)}
	<br>
</div>	
		`),
	),
});




/*********************
*   3d Walkscape
**********************/
CodeEventList.push({
	id: "3dWalkscape",
	startDate: new Date("Oct 8 2016"),
	tags: ["featured", "development", "3d", "interactive"],
	title: "3d Walkscape",
	desc: () => twoColLayout(
		[
			{ hackableHTML: extLink(
				`<img class="--bordered" src="http://sephreed.github.io/portfolio/public/images/3d_walkscape_loop.gif">`,
				"http://sephreed.github.io/walkscape/index.html", 
			)},
		],
		// 
		fromMD(`
**Skills:** Three.js, Blender, Physics Emulation, Procedural Generation, Sketchup

----

**Info:** This was a fairly simple project, testing out Three.js to see what it was capable of.  The answer is quite a lot for anything in a browser.  The project has some very rough physics (the collisions are done with raycasting), perlin noise procedurally generated hills (no random seed), and a few interactive elements (the door to the house and the sphere).  I'd like to get back into this project some day.

---

<div class="Links">
	${extLink(
		"Play with live demo", 
		"http://sephreed.github.io/walkscape/index.html",
	)}
	<br>
	${extLink(
		"Watch screencap", 
		"https://www.youtube.com/watch?v=Cu_6l9h4BIs&t=30s",
	)}
	
</div>
		`),
	),
});

// /*********************
// *   TEMPLATE
// **********************/
// CodeEventList.push({
// 	id: "Stash",
// 	startDate: new Date("Oct 8 2016"),
// 	tags: ["featured", "work", "development", "design"],
// 	title: "",
// 	desc: twoColLayout(
// 		[
// 			{ hackableHTML: extLink(
// 				`<img class="--clip_curf_all_sm --add_bg_light" src="${require('../../../../assets/StashLogo.png')}">`,
// 				"https://stashcrypto.com", 
// 			)},
// 		],
// 		// 
// 		fromMD(`
// **Skills:** 

// ----

// **Info:** 

// ---

// <div class="Links">
// 	${extLink(
// 		"StashCrypto.com", 
// 		"https://stashcrypto.com",
// 	)}
// </div>
// 		`),
// 	),
// });

export default CodeEventList;
