import {div} from "el-tool";
import {ILifeEvent, twoColLayout, extLink, fromMD} from "./LifeEvent"


const BuildEvents: ILifeEvent[] = [];

// ☆

/*********************
*   PINE CONONAGON
**********************/
const glamorShot = require('../../../../assets/Cononagon_Glamor.jpg');
const forestEntrance = require('../../../../assets/Cononagon-Forest_entrance.jpg');
const underworld = require('../../../../assets/Cononagon_underworld(sax).jpg');
const engulfed = require('../../../../assets/Cononagon_engulfed.jpg');
BuildEvents.push({
	id: "Cononagon",
	startDate: new Date("Jan 1 2018"),
	endDate: new Date("Apr 10 2018"),
	tags: ["featured", "social", "leadership", "work", "fabrication", "design", "3d", "interactive", "audio", "writing", "imagery", "development"],
	title: `* Codesigner, Lead Engineer, and Foreman for the "PINE Cononagon"`,
	desc: () => twoColLayout(
		[
			{ hackableHTML: extLink(
				`<img class="--clip_curf_all_sm --add_bg_light" src="${glamorShot}">`,
				glamorShot, 
			)},
			{ hackableHTML: extLink(
				`<img class="--clip_curf_all_sm --add_bg_light" src="${forestEntrance}">`,
				forestEntrance, 
			)},
			{ hackableHTML: extLink(
				`<img class="--clip_curf_all_sm --add_bg_light" src="${underworld}">`,
				underworld, 
			)},
			{ hackableHTML: extLink(
				`<img class="--clip_curf_all_sm --add_bg_light" src="${engulfed}">`,
				engulfed, 
			)},
		],
		// 
		fromMD(`
**Skills:** Team Leading, Structural Engineering, Architecture, Construction, CAD Modeling, Interactive Art, Good Times

----

**Info:** To date (Oct 2018), this project is the pinnacle of my portfolio, as well as Misty's (the co-lead/my girlfriend).  It was a massive multimedia endeavor, spanning three floors, brimming with creativity, and built entirely by volunteers (myself included) on a shoe-string materials budget.  The outside of the piece was meant to look like a pine cone, had a nonagon footprint, and was covered in petals which opened once alit.  The interior had three floors and a secret room.  The second floor was the main entrance, a twinkling forest with an 8ft wide CNC-etched-bark tree in the center and hand-made vines woven around.  Inside the massive center tree was a spiral staircase which led from the mortal realm forest into the after-life underworld which had a cozy den feel and was filled with art pieces dedicated to lost loved ones.  It represented immortality by lore and also housed a live acting group which sent participants on quests related to helping citizens of the afterlife.  The third floor was somewhat hidden as it represented immortality through science, and was decorated as a comfy padded stratosphere staring upwards into fiber-optic constillations spread across the ceiling. Here are the parts I can claim responsibility for:

<table class="table_variant_horizontal_lines">
<tr><th>Skill</th><th>Tasks</th></tr>

<tr><td>Leadership</td><td>
 
- Always being multiple steps ahead on what needs to be done
- Showing people easier ways to do things (being mindful not to puzzle ruin)
- Exemplifying safety and always doing things in ways others could understand
- Being thankful and appreciative of all work or input
- Keeping an eye out for the safety of others
- Making sure to not let people overwork themselves

</td></tr>

<tr><td>Foreman and Engineer</td><td>
 
- Create, maintain, and reference 3d CAD model
- Assurement and testing of sturdiness/strength 
- Planning out and testing build tasks (materials, tools, techniques) before assigning them
- Helping with build tasks when I had the time
- Knowing everything currently being worked on, checking in frequently (and congratulating often)
- Coming early/staying late to do all the worst work that would be cruel to assign

</td></tr>

<tr><td>Co-Designer</td><td>
 
- Came up with thematic inspirations for each of the areas, and wove a story across them
- Decided upon artistic boundaries for the art pieces, soundscape, and live actors
- Came up with lore and backstories for the piece
- Created the overall canvas which the volunteers filled

</td></tr>

<tr><td>Developer</td><td>

- Wrote the software for the LEDs
- Made the proposal website

</td></tr>
</table> 

<small>We managed to complete this project in record time (the event has been running for 20 years) while training newbies, keeping well within budget, and all without any drama.  I'd long since felt capable of leadership roles, but this level of success indicates not only competence, but exceptionalism as a leader.  That being said, I definitely could not have done this without Misty, so please check out her portfolio if you're looking for a super amazing Product/Project Manager, HR, UX Designer, or Creative Director.  She's really great!</small>

---

<div class="Links">
	${extLink(
		"Project Proposal", 
		"https://mistynickle.github.io/DaFT-2018/proposal.html",
	)}
</div>
		`),
	),
});




/*********************
*   PLAYWOOD PALACE
**********************/
BuildEvents.push({
	id: "PlaywoodPalace",
	startDate: new Date("Jan 1 2017"),
	endDate: new Date("Apr 10 2017"),
	tags: ["featured", "social", "leadership", "work", "fabrication", "design", "3d", "interactive", "writing", "imagery", "development"],
	title: `Codesigner, Lead Engineer, and Foreman for the "Playwood Palace"`,
	desc: () => twoColLayout(
		[
			{ hackableHTML: extLink(
				`<img class="--clip_curf_all_sm --add_bg_light" src="http://sephreed.github.io/Playwood_Palace/public/images/2017_by_Mystic%20Loden_big.png">`,
				"http://sephreed.github.io/Playwood_Palace/index.html", 
			)},
			{ hackableHTML: extLink(
				`<img class="--clip_curf_all_sm --add_bg_light" src="http://sephreed.github.io/Playwood_Palace/public/images/glamor/inner_adults.png">`,
				"http://sephreed.github.io/Playwood_Palace/public/images/glamor/inner_adults.png", 
			)},
			{ hackableHTML: extLink(
				`<img class="--clip_curf_all_sm --add_bg_light" src="http://sephreed.github.io/Playwood_Palace/public/images/glamor/dustin_boosh.png">`,
				"http://sephreed.github.io/Playwood_Palace/public/images/glamor/dustin_boosh.png", 
			)},
			{ hackableHTML: extLink(
				`<img class="--clip_curf_all_sm --add_bg_light" src="http://sephreed.github.io/Playwood_Palace/public/images/glamor/collapse.png">`,
				"http://sephreed.github.io/Playwood_Palace/public/images/glamor/collapse.png", 
			)},
		],
		// 
		fromMD(`
**Skills:** Team Leading, Structural Engineering, Construction, CAD Modeling, Interactive Art, Audio Editing, Good Times

----

**Info:** This is my second largest project to date (as of Oct 2018), alongside the co-lead/my girlfried Misty.  It was an enormous three story maze/playscape shaped vaguely like a unicorn, and was built by a volunteer team for the regional Burning Man-inspired event: Flipside.  The winding interior of the first floor consisted of many passages to rooms such as the cafe/bar, a hobbit home, a mini forest with fake campfire, a greek veranda with blacklight chalkboards, and a darkroom  ballpit in space.  Connecting the first and second floor were 1ft tall steps which, alongside the 4ft tall railings, gave participants the perception of being small again.  On the second floor were two secret rooms, one of which was full of treasure and required two people working together to open (the door had a pull string latch with the pulling end hidden far from the door).  From the second floor, participants could get into both of the turrets, each containing an aimable flamethrower.  On the third floor was the crux of the piece, the top of a three-story slide.  Tons of work and volunteer hours went into this piece -- this is what of it I can claim:

<table class="table_variant_horizontal_lines">
<tr><th>Skill</th><th>Tasks</th></tr>

<tr><td>Leadership</td><td>
 
- Always being multiple steps ahead on what needs to be done
- Showing people easier ways to do things (being mindful not to puzzle ruin)
- Exemplifying safety and always doing things in ways others could understand
- Being thankful and appreciative of all work or input
- Keeping an eye out for the safety of others

</td></tr>

<tr><td>Foreman and Engineer</td><td>
 
- Create, maintain, and reference 3d CAD model
- Assurement and testing of sturdiness/strength 
- Planning out and testing build tasks (materials, tools, techniques) before assigning them
- Spending ~30 hours a week fabricating the more challenging projects 
- Knowing everything currently being worked on, checking in frequently (and congratulating often)

</td></tr>

<tr><td>Co-Designer</td><td>
 
- Came up with thematic inspirations for each of the areas
- Created the soundcape
- Co-authored the story in the "childrens" book made for the piece
- Designed the t-shirts, as well as colorizing/photoshopping images for our swag and the "childrens" book
- Created the overall canvas which the volunteers filled

</td></tr>

<tr><td>Developer</td><td>

- Made the proposal website
- Created a budgeting tool which calculated the total price of the piece, as well as the materials list
- Created a media page 

</td></tr>
</table> 

<small>We managed to complete this project in record time (the event had been running for 19 years at this point) while training newbies, keeping well within budget, and with minimal drama.  This was the first time I'd ever been given a position of so much power, and I feel the results were very positive.  That being said, I definitely could not have done this without Misty, so please check out her portfolio if you're looking for a super amazing Product/Project Manager, HR, UX Designer, or Creative Director.  She's really great!</small>

---

<div class="Links">
	${extLink(
		"This site has tons of links to media from the project", 
		"http://sephreed.github.io/Playwood_Palace/index.html",
	)}
</div>
		`),
	),
});


// /*********************
// *   TEMPLATE
// **********************/
// BuildEvents.push({
// 	id: "Stash",
// 	startDate: new Date("Oct 8 2016"),
// 	tags: ["featured", "work", "development", "design"],
// 	title: "",
// 	desc: twoColLayout(
// 		[
// 			{ hackableHTML: extLink(
// 				`<img class="--clip_curf_all_sm --add_bg_light" src="${require('../../../../assets/StashLogo.png')}">`,
// 				"https://stashcrypto.com", 
// 			)},
// 		],
// 		// 
// 		fromMD(`
// **Skills:** 

// ----

// **Info:** 

// ---

// <div class="Links">
// 	${extLink(
// 		"StashCrypto.com", 
// 		"https://stashcrypto.com",
// 	)}
// </div>
// 		`),
// 	),
// });

export default BuildEvents;