import JobEvents from "./JobEvents";
import CodeEvents from "./CodeEvents";
import NpmEvents from "./NpmEvents";
import BuildEvents from "./BuildEvents";
import MusicEvents from "./MusicEvents";
import {ILifeEvent} from "./LifeEvent";


const LifeEventList: ILifeEvent[] = [
	...JobEvents,
	...CodeEvents,
	...NpmEvents,
	...BuildEvents,
	...MusicEvents,
];

LifeEventList.sort((a, b) => {
	const aDate = (a.endDate || a.startDate);
	const bDate = (b.endDate || b.startDate);
	return aDate > bDate ? -1 : (aDate === bDate ? 0 : 1)
})
export default LifeEventList;
