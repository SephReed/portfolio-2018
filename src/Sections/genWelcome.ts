import { create, br, div, span } from "el-tool";

export default function generateWelcome() {
	return div("Welcome", [
		"Hello and welcome to my portfolio.  I find it funny that fully introducing oneself at the head of a portfolio seems so out of order, but if you're in a hurry you can always skip this section and come back later.  ",
		// br(),
		// "",
		span("Highlight", "My name is "),
		span("MyName", "Seph Reed"),
		", and little by little, I've managed to stray from almost any form of group identity.  As a result, I can say with the utmost confidence: ",
		span("Highlight", "You won't meet another person like me.  "),
		"As I hope you'll agree upon seeing my timeline below, this divergence has served me well, resulting in many uniquely inspired works.  But it is often not easy.  ",
		" Thinking outside the box means always having the minority opinion, so persuasion, grit, and tenacity have become my greatest tools.  ",
		create("a", {attr: {href: "https://mycorevalues.app/"}}, `Mycorevalues.app`),
		" quite accurately assesed that ",
		span("Highlight", `Self-Respect, Humor, Integrity, Self-Awareness, and Competency are my core values `),
		"and these values do a great job of describing what my MO is. ",
		// br(),
		"In terms of character, the ",
		create("a", {attr: {href: "https://www.myersbriggs.org/my-mbti-personality-type/mbti-basics/reliability-and-validity.htm?bhcp=1"}}, "Briggs-Myers"),
		" test deduces that ",
		span("Highlight", [
			`my personality is somewhere between `,
			create("a", {attr: {href: "https://www.16personalities.com/intj-personality"}}, `INTJ`),
			" and ",
			create("a", {attr: {href: "https://www.16personalities.com/entj-personality"}}, `ENTJ`),
			". ",
		]), 
		"Both of these rare types are best suited for positions involving higher level/big picture thinking and leadership.  It is in positions such as these that I am happiest, and do the most good. ",
		"One last thing to note is that my skillset is very expansive.  ",
		span("Highlight", `I'm into everything. `),
		"I started coding 18 years ago at age 10, and have always been a tinkerer both digitally and physically.  At this point in my career, there aren't many subjects or skills that I haven't made myself competent in and even less that I have no aim to be. ",
		"Anyways, here's a tagline I quite like: ",
		span("Highlight", "if you're seeking exceptional results, hire exceptional people "),
		"(be them me or not)."
	]);
}
