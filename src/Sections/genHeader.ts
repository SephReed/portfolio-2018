import { create, br, div, span, addClass } from "el-tool";
import TangentAnimator from "./stars";
import resizeHandler from "../Util/resizeHandler";

export default function generateHeader() {
	let canvas: HTMLCanvasElement;

	const thumbzSymbol = div("", {
		innerHTML: `<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="160 95 150 280">
			<path d="M209.72 302.22L302.22 264.57L236.9 105.63L171.57 264.57L264.06 302.22"></path>
			<path d="M209.72 171.57L302.22 209.43L236.9 366.9L171.57 209.43L264.06 171.57"></path>
		</svg>`,
	}).firstChild;

	const isMobile = /Mobi|Android/i.test(navigator.userAgent);

	let bg1: HTMLElement;
	let bg2: HTMLElement;
	const out = div("Header", [
		div("Header-bg", [
			canvas = create("canvas"),
			div("Instructions", [
				div("Instructions-bg"),
				div("Instructions-fg", `${isMobile? "Double-tap" : "Click"} canvas to reset`),
			]),
		]),
		div("Header-fg", [
			bg2 = addClass(thumbzSymbol.cloneNode(true) as any, "logo-bg-2"),
			bg1 = addClass(thumbzSymbol.cloneNode(true) as any, "logo-bg-1"),
			addClass(thumbzSymbol.cloneNode(true) as any, "logo-fg"),
		]),
	]);

	// css animations don't naturally throttle
	const intervalMs = 250;
	const loopLength = 25 * 1000;
	let time = 0;
	setInterval(() => {
		time += intervalMs;
		time %= loopLength;
		const ratio = time/loopLength;
		const opacity = (ratio < 0.5 ? ratio : (1 - ratio)) * 2;
		bg1.style.opacity = opacity + "";
		bg2.style.opacity = (1-opacity) + "";
	}, intervalMs)

	const tangentAnim = new TangentAnimator(canvas);
	resizeHandler(() => tangentAnim.updateSize());

	return out;
}