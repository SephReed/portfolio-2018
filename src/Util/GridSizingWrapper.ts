import { div } from "el-tool";
import observeSize from "../Util/observeSize";

export default class GridSizingWrapper {
	public readonly domNode;
	private content: HTMLElement;
	private heightUpdateFrame: number;
	private paddingRight: number;
	private paddingBottom: number;

	constructor(content: HTMLElement) {
		this.content = content;
		this.domNode = div("GridSizingWrapper", [content]);

		observeSize(content, (height, width) => {
			const widthOver = width % 25;
			if (widthOver !== 0) {
				this.paddingRight = ((this.paddingRight || 0) + widthOver) % 25;
				this.domNode.style.paddingRight = this.paddingRight + "px";
			} else {
				const heightUnder = 25 - (height % 25);
				if (heightUnder !== 0) {
					this.paddingBottom = ((this.paddingBottom || 0) + heightUnder) % 25;
					this.content.style.paddingBottom = this.paddingBottom + "px";
				}
			}
		});
	}
}