export default function addBrowserClassToBody() {
	const addClass = () => {
		let browserName = '';
		const isIE = /*@cc_on!@*/false || !!(document as any).documentMode;
		const isEdge = !isIE && !!(window as any).StyleMedia;
		if(navigator.userAgent.indexOf("Chrome") != -1 && !isEdge) {
		    browserName = 'chrome';
		} else if(navigator.userAgent.indexOf("Safari") != -1 && !isEdge) {
		    browserName = 'safari';
		} else if(navigator.userAgent.indexOf("Firefox") != -1 ) {
		    browserName = 'firefox';
		} else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!(document as any).documentMode == true )) {
		    browserName = 'ie';
		} else if(isEdge) {
		    browserName = 'edge';
		} else {
		   browserName = 'unknown';
		}
		document.body.classList.add(`--browser_${browserName}`);
	};

	if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", addClass);
	} else {  
	  addClass();
	}
};