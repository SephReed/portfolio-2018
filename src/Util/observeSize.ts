import {div} from "el-tool";

let resizeSensorNode: HTMLElement;
function getnitResizeSensorNode() {
  if (resizeSensorNode === undefined) {
    const style = document.createElement("style");
    style.innerHTML = [
      `.ResizeSensors, .ResizeSensors>.Sensor {`,
      "  position: absolute; display: block;",
      "  left: 0; top: 0; right: 0; bottom: 0; z-index: -1;",
      "  visibility: hidden; overflow: scroll;  pointer-events: none;",
      `}`,
      `.ResizeSensors>.Sensor.shrink>.Trigger {`,
      "  width: 200%; height: 200%;",
      `}`,
      `.ResizeSensors>.Sensor.expand>.Trigger {`,
      "  width: 100000px; height: 100000px;",
      `}`,
    ].join("\n");
    document.head.appendChild(style);

    resizeSensorNode = div("ResizeSensors", [
      div("Sensor expand", [div("Trigger")]),
      div("Sensor shrink", [div("Trigger")]),
    ]);
  }
  return resizeSensorNode;
}

// TODO: should return a disposable
export default function observeSize(target: HTMLElement, cb: (height: number, width: number) => void) {
  if (!target) return;
  const moddableTarget: any = target;
  if (moddableTarget.resizeListeners) {
    moddableTarget.resizeListeners.push(cb);
    return;
  } else {
    moddableTarget.resizeListeners = [];
    moddableTarget.resizeListeners.push(cb);    
  }

  const resizeSensor = getnitResizeSensorNode().cloneNode(true) as HTMLElement;
  target.appendChild(resizeSensor);

  const expand = resizeSensor.firstChild as HTMLElement;
  const expandTrigger = expand.firstChild as HTMLElement;
  const shrink = resizeSensor.childNodes[1] as HTMLElement;

  const resetTriggers = () => {
    expand.scrollLeft = 100000;
    expand.scrollTop = 100000;
    shrink.scrollLeft = 100000;
    shrink.scrollTop = 100000;
  };

  let lastWidth = target.offsetWidth;
  let lastHeight = target.offsetHeight;
  let frameRequest: number;

  const onScroll = () => {
    resetTriggers();
    if (frameRequest) { return; }
    frameRequest = requestAnimationFrame(() => {
      const computedStyle = window.getComputedStyle(target);
      // const newWidth = target.offsetWidth;
      // const newHeight = target.offsetHeight;
      const newWidth = parseFloat(computedStyle.getPropertyValue("width").replace("px", ""));
      const newHeight = parseFloat(computedStyle.getPropertyValue("height").replace("px", ""));
      if (newWidth !== lastWidth || newHeight !== lastHeight) {
        lastWidth = newWidth;
        lastHeight = newHeight;
        moddableTarget.resizeListeners.forEach((listener) => listener(lastHeight, lastWidth));
      }
      frameRequest = undefined;
    });
  };

  expand.addEventListener("scroll", () => {
    // console.log("expand"); 
    onScroll();
  });
  shrink.addEventListener("scroll", () => {
    // console.log("shrink"); 
    onScroll();
  });

  requestAnimationFrame(() => requestAnimationFrame(() => 
    resetTriggers(),
  ));
}
