import observeSize from "./observeSize";

export default function onThrashingEnd(domNode: HTMLElement, cb: () => void, timeout: number = 300) {
	let thrashingTimeout: number;
	const resetTimeout = () => {
		window.clearTimeout(thrashingTimeout);
		thrashingTimeout = window.setTimeout(() => {
			thrashingTimeout = undefined;
			// should dispose of size observer
			cb();
		}, timeout)
	}

	observeSize(domNode, () => {
		// console.log("body thrash");
		if (thrashingTimeout) { resetTimeout(); }
	});
	resetTimeout();
}

export function tempAddThrashingClass(domNode: HTMLElement, timeout?: number) {
	domNode.classList.add("--is_thrashing");
	onThrashingEnd(domNode, () => domNode.classList.remove("--is_thrashing"), timeout);
}

export function addFirstLoadThrashingClass(timeout?: number) {
	const observeBody = () => {
		if (document.body.classList.contains("--is_thrashing") === false) {
			console.error(`"--is_thrashing" should be added to body html`);
		}
		tempAddThrashingClass(document.body, timeout);
	};

	if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", observeBody);
	} else {  
	  observeBody();
	}
}