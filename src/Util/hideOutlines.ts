export default function hideOutlines() {
	let outlinesShowing = true;
	const showOutlines = (onOff: boolean) => {
		if (onOff === outlinesShowing) { return; }
		outlinesShowing = onOff;
		if (outlinesShowing) {
			document.body.classList.remove("--hide_outlines");
		} else {
			document.body.classList.add("--hide_outlines");
		}
	}

	window.addEventListener("mousemove", () => showOutlines(false))
	window.addEventListener("mousedown", () => showOutlines(false))

	window.addEventListener("keydown", (event) => {
		if (event.key === "Tab" || event.key.includes("Arrow")) {
			showOutlines(true);
		}
	});

	const addCss = () => {
		const style = document.createElement("style");
		style.id = "hideOutlines.ts";
		style.innerHTML = [
			`body.--hide_outlines input,`,
			`body.--hide_outlines textarea,`,
			`body.--hide_outlines button,`,
			`body.--hide_outlines a {`,
			`	outline: none;`,
			`}`,
		].join("");
		document.head.appendChild(style);
	}
	
	if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", addCss);
	} else {  
	  addCss();
	}
}