import {append, Innards, a, IAnchorProps} from "el-tool";

import Observables, {BasicObservableClass} from "vanilla-observables";

type ObservableProps = "location" | "pathname" | "hash" | "searchArgs";
class History extends BasicObservableClass<ObservableProps> {
	public location = this.observable<string>("location", "ro");
	public pathname = this.observable<string>("pathname", "ro");
	public hash = this.observable<string>("hash", "ro");
	public searchArgs = this.observable<{[key: string]: string}>("searchArgs", "ro");
	private firstPath = true;
	
	constructor() {
		super();
		this.observables.set("location", window.location.href);

		this.location.observe((newLoc) => {
			const url = document.createElement('a');
			url.href = newLoc;

			const searchArgs = {};
			(url.search.replace(/^\?/, ""))
			.split(/&/g)
			.forEach((keyValStr) => {
				const keyVal = keyValStr.split("=");
				searchArgs[keyVal[0]] = keyVal[1];
			});

			this.observables.set("pathname", url.pathname);
			const hash = url.hash.replace(/^#/, "").replace(/\?[\s\S]*/, "");
			// if (hash !== this.hash()) {
				this.observables.set("hash", hash);
			// }
			this.observables.set("searchArgs", searchArgs);
		});

		this.hash.observe((newHash) => {
			if (!newHash || !newHash.length) { return; }
			let tryCount = 0;
			let waitMs = 5;
			const tryScroll = () => {
				const target = document.getElementById(newHash);
				if (target) {
					if (this.firstPath) {
						target.scrollIntoView();
						this.firstPath = false;
					} else {
						target.scrollIntoView({behavior: "smooth"})
					}
				} else if (tryCount < 20) {
					tryCount++;
					waitMs *= 2;
					setTimeout(() => tryScroll(), waitMs);
				}
			}
			tryScroll();
		});

		window.addEventListener("popstate", () => {
			this.observables.set("location", window.location.href);
		});
	}

	public push(href: string) {
		window.history.pushState(null, null, href);
		this.observables.set("location", href);
	}
}
const GlobalHistory = new History();
export default GlobalHistory;

export function createLink(className: string, props: IAnchorProps, innards?: Innards, nonNewTabHref?: string): HTMLAnchorElement {
	const link = a(className, props, innards);

	const cancelNonNewTabs = (event: MouseEvent | KeyboardEvent) => {
		if (event.ctrlKey || event.metaKey) { return; }
		event.preventDefault();
		GlobalHistory.push(nonNewTabHref || link.href);
		return false;
	}

	link.addEventListener("click", (ev) => cancelNonNewTabs(ev));
	link.addEventListener("keydown", (ev) => {
		if (ev.key === "Enter") { cancelNonNewTabs(ev); }
	});

	return link;
}