import {addClass, div, create, Child} from "el-tool";
import resizeHandler from "./resizeHandler";

import "./DropDown.scss";

// children.map((child) => create("button", {class: "DropDownItem"}, [child]))

export default class DropDown {
	public readonly domNode: HTMLElement;
	private selectedChild: HTMLElement;
	private mightJustBeClick: boolean;
	private fullHeight: number;
	private selectionObservers: Array<(selectedChild: HTMLElement) => void>;
	private fullHeightObservers: Array<(fullHeight: number) => void>;
	private isOpenObservers: Array<(isOpen: boolean) => void>;

	constructor(children: Child[], selectedItem?: HTMLElement) {
		this.domNode = div("DropDown", {
			// onClick: (ev) => this.onClick(ev),
			onMouseDown: (ev) => this.onMouseDown(ev),
		}, children);
		this.domNode.addEventListener("click", (ev) => this.onClick(ev), true);
		this.selectionObservers = [];
		this.fullHeightObservers = [];
		this.isOpenObservers = [];
		this.selectChild(selectedItem || this.domNode.firstChild as any);
		resizeHandler(() => {
			this.fullHeight = 0;
			Array.from(this.domNode.children).forEach((child) => this.fullHeight += child.scrollHeight);
			this.fullHeightObservers.forEach((cb) => cb(this.fullHeight));
			this.updateHeight();
		});
		document.body.addEventListener("click", (event) => {
			if (this.isOpen && this.domNode.contains(event.target as any) === false) {
				this.setOpen(false);
			}
		})
	}

	// TODO: add observers
	public observeFullHeight(cb: (fullHeight: number) => void) {
		this.fullHeightObservers.push(cb);
		cb(this.fullHeight);
	}

	public observeIsOpen(cb: (isOpen: boolean) => void) {
		this.isOpenObservers.push(cb);
		cb(this.isOpen);
	}

	public observeSelection(cb: (selectedChild: HTMLElement) => void) {
		this.selectionObservers.push(cb);
		cb(this.selectedChild);
	}

	private get isOpen() {
		return this.domNode.classList.contains("--drop_down_open");
	}

	private onClick(event: MouseEvent) {
		if (this.isOpen && this.mightJustBeClick === false) {
			if (event.target !== this.domNode) {
				this.selectChild(event.target as any);
			}
			this.setOpen(false);
		} else {
			// stop buttons or links from getting click
			event.stopPropagation();
			event.preventDefault();
		}
		this.mightJustBeClick = false;
	}

	private onMouseDown(event: MouseEvent) {
		if (this.isOpen === false) {
			this.setOpen(true);
			this.mightJustBeClick = true;
			setTimeout(() => this.mightJustBeClick = false, 500);
		}
	}

	private setOpen(isOpen: boolean | "flip") {
		if (isOpen === "flip") {
			isOpen = !this.isOpen;
		}
		if (isOpen) {
			this.domNode.classList.add("--drop_down_open")
		} else {
			this.domNode.classList.remove("--drop_down_open")
		}
		this.isOpenObservers.forEach((cb) => cb(this.isOpen));
		this.updateHeight();
	}

	public selectChild(child: HTMLElement) {
		if (this.selectedChild) {
			this.selectedChild.classList.remove("--selected_item");
		}
		this.selectedChild = addClass(child, "--selected_item");
		this.selectionObservers.forEach((cb) => cb(this.selectedChild));
	}

	private updateHeight() {
		if (this.isOpen) {
			this.domNode.style.height = this.fullHeight + "px";
		} else {
			this.domNode.style.height = this.selectedChild.scrollHeight + "px";
		}
	}
}