// import {div, removeChildren} from "el-tool";
// import "./ScalableCurfAug.scss";

// export type Curf = {height: number; width: number};

// export interface IArgs {
// 	curfNw?: Curf;
// 	curfNe?: Curf;
// 	curfSw?: Curf;
// 	curfSe?: Curf;
// 	contentElement?: HTMLElement;
// }

// export interface IStrokeAndFill {
// 	svg: SVGElement;
// 	stroke: SVGPathElement;
// 	fill: SVGPathElement;
// }

// const SVG_NS = "http://www.w3.org/2000/svg";
// const fillVal = "1fr";

// export default class ScalableCurfAug {
// 	public readonly domNode: HTMLElement;
// 	private args: IArgs;

// 	public readonly bgDomNode: HTMLElement;
// 	private svgNw: IStrokeAndFill;
// 	private svgNe: IStrokeAndFill;
// 	private svgSw: IStrokeAndFill;
// 	private svgSe: IStrokeAndFill;


// 	public constructor(domNode: HTMLElement, args: IArgs) {
// 		this.domNode = domNode;
// 		this.args = args;
// 		this.bgDomNode = document.createElement("div");
// 		this.bgDomNode.classList.add("ScalableCurfAug");

// 		// ["Nw", "Ne", "Sw", "Se"].forEach((edge) => {
// 		// 	const svg = document.createElementNS(SVG_NS, "svg");
// 		// 	const className = `Curf${edge}`;
// 		// 	svg.style.gridArea = className;
// 		// 	svg.classList.add(className);
// 		// 	const stroke = document.createElementNS(SVG_NS, "path");
// 		// 	stroke.classList.add(className + "Stroke");
// 		// 	const fill = document.createElementNS(SVG_NS, "path");
// 		// 	fill.classList.add(className + "Fill");
// 		// 	this[`svg${edge}`] = {svg, stroke, fill};
// 		// 	this.bgDomNode.appendChild(svg);
// 		// });

// 		// ["N", "E", "S", "W", "Center"].forEach((divName) => {
// 		// 	const div = document.createElement("div");
// 		// 	const className = (divName === "Center") ? "FillCenter" : `Edge${divName}`;
// 		// 	div.className = className;
// 		// 	div.style.gridArea = className;
// 		// 	this.bgDomNode.appendChild(div);
// 		// });

		
// 		domNode.appendChild(this.bgDomNode);

// 		this.redraw();
// 	}

// 	public patchArgs(args: IArgs) {
// 		for (let key in args) {
// 			this.args[key] = args[key];
// 		}
// 		this.redraw();
// 	}

// 	private redraw() {
// 		const {curfNw, curfNe, curfSw, curfSe } = this.args;

// 		const getSizes = (curfA: Curf, curfB: Curf, side: "width" | "height") => {
// 			if (!curfA && !curfB) {
// 				return [];
// 			} else if (curfA && curfB && curfA[side] !== curfB[side]) {
// 				const out = [curfA[side], curfB[side]].sort((a, b) => a-b);
// 				out[1] -= out[0];
// 				return out;
// 			}
// 			return [(curfA || curfB)[side]];
// 		}

// 		const columns = [
// 			...getSizes(curfNw, curfSw, "width").map((width) => width+"px"),
// 			fillVal,
// 			...getSizes(curfNe, curfSe, "width").reverse().map((width) => width+"px"),
// 		];

// 		const rows = [
// 			...getSizes(curfNw, curfNe, "height").map((width) => width+"px"),
// 			fillVal,
// 			...getSizes(curfSw, curfSe, "height").reverse().map((width) => width+"px"),
// 		];
		
// 		const domNode = this.bgDomNode;
// 		domNode.style.gridTemplateColumns = columns.join(" ");
// 		domNode.style.gridTemplateRows = rows.join(" ");

// 		removeChildren(domNode);

// 		this.addFills(rows, columns);
// 		this.addCurfs(rows, columns);		
// 	}

// 	private addFills(rows: string[], columns: string[]) {
// 		const {curfNw, curfNe, curfSw, curfSe } = this.args;
// 		const domNode = this.bgDomNode;

// 		const centerRow = rows.indexOf(fillVal);
// 		const centerCol = columns.indexOf(fillVal);

// 		const getPoint = (
// 			direction: "start" | "end",
// 			inlineCurf: Curf,
// 			linePoints: string[],
// 			currentPoint: number,
// 		):number => {
// 			const isStart = direction === "start";
// 			currentPoint++;
// 			if (!inlineCurf) {
// 				return isStart ? 1 : linePoints.length + 1;
// 			} else if ((inlineCurf.width + "px") === linePoints[isStart ? 0 : linePoints.length-1]) {
// 				return currentPoint + (isStart ? -1 : 1);
// 			}
// 			return currentPoint;
// 		}

// 		const mainFill = div("MainFill", {
// 			style: {
// 				"grid-column": `1 / ${columns.length + 1}`,
// 				"grid-row":  `${centerRow+1}`,
// 			}
// 		});
// 		if (centerRow !== 0) {
// 			const start = getPoint("start", curfNw, columns, centerCol);
// 			const end = getPoint("end", curfNe, columns, centerCol);
// 			div("TopFill", {
// 				appendTo: domNode,
// 				classList: [
// 					start === 0 ? "toLeft" : null,
// 					end === columns.length ? "toRight" : null,
// 				].filter((it) => !!it),
// 				style: {
// 					"grid-column": `${start} / ${end}`,
// 					"grid-row": `1 / ${centerRow+1}`,
// 				}
// 			})
// 		} else {
// 			mainFill.classList.add("toTop");
// 		}

// 		domNode.appendChild(mainFill);

// 		if (centerRow !== rows.length-1) {
// 			const start = getPoint("start", curfSw, columns, centerCol);
// 			const end = getPoint("end", curfSe, columns, centerCol);
// 			div("BottomFill", {
// 				appendTo: domNode,
// 				classList: [
// 					start === 0 ? "toLeft" : null,
// 					end === columns.length ? "toRight" : null,
// 				].filter((it) => !!it),
// 				style: {
// 					"grid-column": `${start} / ${end}`,
// 					"grid-row": `${centerRow+2} / ${rows.length+1}`,
// 				}
// 			})
// 		} else {
// 			mainFill.classList.add("toBottom");
// 		}
// 	}

// 	private addCurfs(rows: string[], columns: string[]) {
// 		const {curfNw, curfNe, curfSw, curfSe } = this.args;
// 		const domNode = this.bgDomNode;

// 		const getSpan = (curf: Curf, inlineCurf: Curf, side: "width" | "height") => {
// 			if (inlineCurf && inlineCurf[side] < curf[side]) {
// 				return 2;
// 			} 
// 			return 1;
// 		}

// 		const addCurf = (curf: Curf, inRowCurf: Curf, inColCurf) => {
// 			if (!curf) { return; }
// 			const rowSpan = getSpan(curf, inRowCurf, "width");
// 			const colSpan = getSpan(curf, inColCurf, "height");

// 			const {width, height} = curf;

// 			const isTopRow = (curf === curfNw || curf === curfNe);
// 			const isLeftCol = (curf === curfNw || curf === curfSw);

// 			const svg = document.createElementNS(SVG_NS, "svg");
// 			// svg.classList.add("CurfAugBorder");
// 			svg.setAttribute("width", width + "px");
// 			svg.setAttribute("height", height + "px");
// 			svg.setAttribute("viewport", `0 0 ${width} ${height}`);
// 			svg.style.gridColumn = `${isLeftCol ? 1 : ((columns.length+1) - colSpan)} / span ${colSpan}`;
// 			svg.style.gridRow = `${isTopRow ? 1 : ((rows.length+1) - rowSpan)} / span ${rowSpan}`;

// 			const stroke = document.createElementNS(SVG_NS, "path");
// 			const x1 = isTopRow ? 0 : 1;
// 			const y1 = isLeftCol ? 1 : 0;
// 			const x2 = (x1+1)%2;
// 			const y2 = (y1+1)%2;
// 			stroke.setAttribute("d", [
// 				`M ${x1 * width} ${y1 * height}`,
// 				`L ${x2 * width} ${y2 * height}`,
// 			].join(" "));
// 			svg.appendChild(stroke);

// 			domNode.appendChild(svg);
// 		}

// 		addCurf(curfNw, curfNe, curfSw);
// 		addCurf(curfNe, curfNw, curfSe);
// 		addCurf(curfSw, curfSe, curfNw);
// 		addCurf(curfSe, curfSw, curfNe);
// 	}
// }