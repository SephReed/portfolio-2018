var path = require('path');
var webpack = require('webpack');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const sourcePath = path.join(__dirname, "./");
const outPath = path.join(__dirname, "./dist");

module.exports = {
		context: sourcePath,
		entry: {
			portfolio: './portfolio.ts',
			resume: "./resume.ts",
			coverLetter: "./coverLetter.ts",
		},
		output: {
        filename: "./dist/[name].bundle.js" 
		},
		resolve: {
			extensions: [".js", ".jsx", ".ts", ".tsx"],
		},
		module: {
				rules: [
					{
            test: /\.ts?$/,
            exclude: /\.(d|test)\.ts$/,
            use: ["awesome-typescript-loader"],
        	},{
            test: /\.scss$/,
            use: [
                "style-loader", // creates style nodes from JS strings
                "css-loader", // translates CSS into CommonJS
                "sass-loader" // compiles Sass to CSS, using Node Sass by default
            ]
        	},{
            test: /\.(png|jpg|jpeg|gif)$/,
            loader: 'url-loader?limit=10000&name=./assets/images/[name].[ext]'
        	}
				]
		},
		stats: {
				colors: true
		},
		plugins: [
			new HtmlWebpackPlugin({
					template: "./index.html",
					chunks: ["portfolio"],
			}),
			new HtmlWebpackPlugin({
					template: "./resume.html",
					filename: "./resume",
					chunks: ["resume"],
			}),
			new HtmlWebpackPlugin({
					template: "./coverLetter.html",
					filename: "./coverLetter",
					chunks: ["coverLetter"],
			}),
		],
		devServer: {
			contentBase: sourcePath,
			disableHostCheck: true,
		},
};
